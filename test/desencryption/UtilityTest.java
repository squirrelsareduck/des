/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desencryption;

import org.apache.commons.codec.binary.Hex;
import java.math.BigInteger;
import java.util.Scanner;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Andrew
 */
public class UtilityTest
{
    
    public UtilityTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }

    /**
     * Test of performPermutations method, of class Utility.
     */
    @Test
    public void testPerformPermutations()
    {
        System.out.println("performPermutations");
        byte[] originalByteArray = {(byte)1,(byte)1,(byte)1,(byte)1,(byte)1,(byte)1,(byte)1,(byte)1};
        String inputPermutationTextFile = "testReversal.txt";
        Utility instance = new Utility();
        byte[] expResult = {(byte)128,(byte)128,(byte)128,(byte)128,(byte)128,(byte)128,(byte)128,(byte)128};
        byte[] result = instance.performPermutations(originalByteArray, inputPermutationTextFile);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of getBit method, of class Utility.
     */
    @Test
    public void testGetBit()
    {
        System.out.println("getBit");
        byte zero = (byte)0;
        byte eight = (byte)8;
        byte[] arrayOfInterest = new byte[4];
        arrayOfInterest[0] = eight;
        arrayOfInterest[1] = zero;
        arrayOfInterest[2] = eight;
        arrayOfInterest[3] = zero;
     
        int index1 = 23;
        int index2 = 20;
        Utility instance = new Utility();
        int result1 = instance.getBit(arrayOfInterest, index1);
        int result2 = instance.getBit(arrayOfInterest,index2);
        assert(result1==0);
        assert(result2==1);
    }

    /**
     * Test of setBit method, of class Utility.
     */
    @Test
    public void testSetBit()
    {
        System.out.println("setBit");
        byte zero = (byte)0;
        byte eight = (byte)8;
        byte[] arrayOfInterest = new byte[4];
        arrayOfInterest[0] = eight;
        arrayOfInterest[1] = zero;
        arrayOfInterest[2] = eight;
        arrayOfInterest[3] = zero;
     
        System.out.println(Hex.encodeHexString(arrayOfInterest));   
        int index = 23;
        Utility instance = new Utility();
        instance.setBit(arrayOfInterest, index);
        System.out.println(Hex.encodeHexString(arrayOfInterest));
        assert(arrayOfInterest[0] == (byte)8);
        assert(arrayOfInterest[1] == zero);
        assert(arrayOfInterest[2] == (byte)9);
        assert(arrayOfInterest[3] == zero);
    }

    /**
     * Test of clearBit method, of class Utility.
     */
    @Test
    public void testClearBit()
    {
        System.out.println("clearBit");
        byte zero = (byte)0;
        byte eight = (byte)8;
        byte[] arrayOfInterest = new byte[4];
        arrayOfInterest[0] = eight;
        arrayOfInterest[1] = zero;
        arrayOfInterest[2] = eight;
        arrayOfInterest[3] = zero;
     
        System.out.println(Hex.encodeHexString(arrayOfInterest));   
        int index = 20;
        Utility instance = new Utility();
        instance.clearBit(arrayOfInterest, index);
        System.out.println(Hex.encodeHexString(arrayOfInterest));
        assert(arrayOfInterest[2] == zero);
    }

    /**
     * Test of bytes28BitsRotateLeft method, of class Utility.
     */
    @Test
    public void testBytes28BitsRotateLeft()
    {
        System.out.println("bytes28BitsRotateLeft");
        byte[] previousBytes = {(byte)129,(byte)2,(byte)4,(byte)20};
        int numberOfBitsToRotateBy=1;
        Utility instance = new Utility();
        byte[] expResult = {(byte)2,(byte)4,(byte)8,(byte)48};
        byte[] result = instance.bytes28BitsRotateLeft(previousBytes, numberOfBitsToRotateBy);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of joinBitsInByteArrays method, of class Utility.
     */
    @Test
    public void testJoinBitsInByteArrays()
    {
        System.out.println("joinBitsInBytes");
        byte[] firstInput = {(byte)0,(byte)5};
        byte[] secondInput = {(byte)7,(byte)3};
        int firstLength = 16;
        int secondLength = 16;
        Utility instance = new Utility();
        byte[] expResult = {(byte)0,(byte)5,(byte)7,(byte)3};
        byte[] result = instance.joinBitsInByteArrays(firstInput, secondInput, firstLength, secondLength);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of scanInTextFile method, of class Utility.
     */
    @Test
    public void testScanInTextFile()
    {
        System.out.println("scanInTextFile");
        String textFile = "testInputFile.txt";
        Utility instance = new Utility();
        int expResult = 64;
        Scanner result = instance.scanInTextFile(textFile);
        int counter=0;
        int prevInt=0;
        while(result.hasNextInt())
        {
            int newInt=result.nextInt();
            if(newInt<prevInt)
                fail();
            prevInt = newInt;
            counter++;
        }
        assertEquals(expResult, counter);
    }
}
