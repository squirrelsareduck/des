/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desencryption;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Andrew
 */
public class KeyGeneratorTest
{

    public KeyGeneratorTest()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    /**
     * Test of performPC1 method, of class KeyGenerator.
     */
    @Test
    public void testPerformPC1()
    {
        System.out.println("performPC1");
        byte[] key56Bits =
        {
            (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2
        };
        KeyGenerator instance = new KeyGenerator();
        byte[] expResult =
        {
            (byte) 0, (byte) 0, (byte) 0, (byte) 15, (byte) 240, (byte) 0, (byte) 0
        };
        byte[] result = instance.performPC1(key56Bits);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of extractBits method, of class KeyGenerator.
     */
    @Test
    public void testExtractBits()
    {
        System.out.println("extractBits");
        byte[] byteToExtractFrom =
        {
            (byte) 2, (byte) 17, (byte) 134, (byte) 2, (byte) 2, (byte) 2, (byte) 2
        };
        int startIndex = 8;
        int length = 12;
        KeyGenerator instance = new KeyGenerator();
        byte[] expResult =
        {
            (byte) 17, (byte) 128
        };
        byte[] result = instance.extractBits(byteToExtractFrom, startIndex, length);
        assertArrayEquals(expResult, result);

        int startIndex2 = 8;
        int length2 = 24;
        byte[] expResult2 =
        {
            (byte) 17, (byte) 134, (byte) 2
        };
        byte[] result2 = instance.extractBits(byteToExtractFrom, startIndex2, length2);
        assertArrayEquals(expResult2, result2);
    }

    /**
     * Test of joinTransformationInputs method, of class KeyGenerator.
     */
    @Test
    public void testJoinTransformationInputs()
    {
        System.out.println("joinTransformationInputs");
        byte[][] leftTransformationInputs =
        {
            {(byte) 1, (byte) 2, (byte) 5, (byte) 8},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 8},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 8},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 8},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 8},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 8},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 8},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 8},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 8},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 8},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 8},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 8},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 8},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 8},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 8},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 8}
        };

        byte[][] rightTransformationInputs =
        {
            {(byte) 7, (byte) 3, (byte) 9, (byte) 4},
            {(byte) 7, (byte) 3, (byte) 9, (byte) 4},
            {(byte) 7, (byte) 3, (byte) 9, (byte) 4},
            {(byte) 7, (byte) 3, (byte) 9, (byte) 4},
            {(byte) 7, (byte) 3, (byte) 9, (byte) 4},
            {(byte) 7, (byte) 3, (byte) 9, (byte) 4},
            {(byte) 7, (byte) 3, (byte) 9, (byte) 4},
            {(byte) 7, (byte) 3, (byte) 9, (byte) 4},
            {(byte) 7, (byte) 3, (byte) 9, (byte) 4},
            {(byte) 7, (byte) 3, (byte) 9, (byte) 4},
            {(byte) 7, (byte) 3, (byte) 9, (byte) 4},
            {(byte) 7, (byte) 3, (byte) 9, (byte) 4},
            {(byte) 7, (byte) 3, (byte) 9, (byte) 4},
            {(byte) 7, (byte) 3, (byte) 9, (byte) 4},
            {(byte) 7, (byte) 3, (byte) 9, (byte) 4},
            {(byte) 7, (byte) 3, (byte) 9, (byte) 4}
        };
        KeyGenerator instance = new KeyGenerator();
        byte[][] expResult = 
        {
            {(byte) 1, (byte) 2, (byte) 5, (byte) 0, (byte) 112, (byte) 48, (byte) 144},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 0, (byte) 112, (byte) 48, (byte) 144},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 0, (byte) 112, (byte) 48, (byte) 144},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 0, (byte) 112, (byte) 48, (byte) 144},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 0, (byte) 112, (byte) 48, (byte) 144},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 0, (byte) 112, (byte) 48, (byte) 144},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 0, (byte) 112, (byte) 48, (byte) 144},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 0, (byte) 112, (byte) 48, (byte) 144},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 0, (byte) 112, (byte) 48, (byte) 144},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 0, (byte) 112, (byte) 48, (byte) 144},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 0, (byte) 112, (byte) 48, (byte) 144},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 0, (byte) 112, (byte) 48, (byte) 144},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 0, (byte) 112, (byte) 48, (byte) 144},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 0, (byte) 112, (byte) 48, (byte) 144},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 0, (byte) 112, (byte) 48, (byte) 144},
            {(byte) 1, (byte) 2, (byte) 5, (byte) 0, (byte) 112, (byte) 48, (byte) 144},
        };
        byte[][] result = instance.joinTransformationInputs(leftTransformationInputs, rightTransformationInputs);
        assertArrayEquals(expResult, result);
    }
}
